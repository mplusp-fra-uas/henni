/*
 * Strassenbahn.h
 *
 *  Created on: 17.07.2018
 *      Author: Hendrik
 */

#ifndef STRASSENBAHN_H_
#define STRASSENBAHN_H_

#include "Fahrgast.h"

class Strassenbahn {

private:
	int Nummer;
	int anzFahrgast;
	int max;
	Fahrgast *Fahrgaeste;

public:
	Strassenbahn(const Strassenbahn& s);
	Strassenbahn(int n, int m);
	Strassenbahn& operator=(const Strassenbahn& s);
	virtual ~Strassenbahn();
	void fahren();
	void halten();
	int getNummer();
	void einsteigen(Fahrgast fg);
	Fahrgast getFahrgast(int pos);
	int getAnzFahrgast();

};

#endif /* STRASSENBAHN_H_ */
