/*
 * Person.cpp
 *
 *  Created on: 17.07.2018
 *      Author: Hendrik
 */

#include "Person.h"
#include <string>
using namespace std;

Person::Person() {
	Name = "Jane Doe";
}

string Person::getName() {
	return Name;
}

void Person::setName(string n) {
	Name = n;
}

