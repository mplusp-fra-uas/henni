#include <iostream>
#include "Kontrolleur.h"

using namespace std;

int main() {
	Strassenbahn bahn(12, 120);
	Kontrolleur kontrolleur;
	kontrolleur.setName("Mike");

	Fahrgast john;
	john.setName("John Schnee");
	Fahrgast henni;
	henni.kaufTicket();
	henni.setName("Hennilein");
	Fahrgast cacace;
	cacace.setName("Cacaceeeeee");

	bahn.einsteigen(john);
	cout << john.getName() << " ist eingestiegen" << endl;
	bahn.einsteigen(henni);
	cout << henni.getName() << " ist eingestiegen" << endl;
	bahn.einsteigen(cacace);
	cout << cacace.getName() << " ist eingestiegen" << endl;

	bahn.fahren();
	kontrolleur.kontrollieren(bahn);
	bahn.halten();
}
