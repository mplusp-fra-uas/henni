/*
 * Kontrolleur.h
 *
 *  Created on: 17.07.2018
 *      Author: Hendrik
 */

#ifndef KONTROLLEUR_H_
#define KONTROLLEUR_H_

#include "Person.h"
#include "Strassenbahn.h"
#include <string>
using namespace std;

class Kontrolleur : public Person {

public:
	Kontrolleur();
	void kontrollieren(Strassenbahn bahn);
};

#endif /* KONTROLLEUR_H_ */
