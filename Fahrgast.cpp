/*
 * Fahrgast.cpp
 *
 *  Created on: 17.07.2018
 *      Author: Hendrik
 */

#include "Fahrgast.h"
using namespace std;

Fahrgast::Fahrgast(bool ticket) {
	hatTicket = ticket;
	kontrolliert = false;
}

Fahrgast::Fahrgast() {
	kontrolliert = false;
	hatTicket = false;
}

void Fahrgast::kaufTicket() {
	hatTicket = true;
}

void Fahrgast::setKontrolliert(bool b) {
	kontrolliert = b;
}

bool Fahrgast::getKontrolliert() {
	return kontrolliert;
}

bool Fahrgast::getHatTicket() {
	return hatTicket;
}

