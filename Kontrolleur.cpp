/*
 * Kontrolleur.cpp
 *
 *  Created on: 17.07.2018
 *      Author: Hendrik
 */

#include "Kontrolleur.h"
#include <string>
#include <iostream>

using namespace std;

Kontrolleur::Kontrolleur() {

}

void Kontrolleur::kontrollieren(Strassenbahn bahn) {
	for(int i = 0; i < bahn.getAnzFahrgast(); i++) {
		if(bahn.getFahrgast(i).getHatTicket() == false) {
			cout << this->getName() << " hat " << bahn.getFahrgast(i).getName() << " ohne Fahrkarte erwischt." << endl;
		}
	}
}
