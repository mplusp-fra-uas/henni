/*
 * Person.h
 *
 *  Created on: 17.07.2018
 *      Author: Hendrik
 */

#ifndef PERSON_H_
#define PERSON_H_

#include <string>
using namespace std;

class Person {
private:
	string Name;
public:
	Person();
	string getName();
	void setName(string n);
};

#endif /* PERSON_H_ */
