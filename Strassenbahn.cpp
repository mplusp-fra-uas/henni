/*
 * Strassenbahn.cpp
 *
 *  Created on: 17.07.2018
 *      Author: Hendrik
 */

#include "Strassenbahn.h"
#include <iostream>

using namespace std;

Strassenbahn::Strassenbahn(int n, int m) {
	Nummer = n;
	max = m;
	anzFahrgast = 0;
	Fahrgaeste = new Fahrgast[max];
}

Strassenbahn::Strassenbahn(const Strassenbahn& s) {
	Nummer = s.Nummer;
	max = s.max;
	anzFahrgast = s.anzFahrgast;
	Fahrgaeste = new Fahrgast[max];
	for(int i = 0; i < anzFahrgast; i++) {
		Fahrgaeste[i] = s.Fahrgaeste[i];
	}
}

Strassenbahn& Strassenbahn::operator=(const Strassenbahn& s) {
	Nummer = s.Nummer;
	max = s.max;
	anzFahrgast = s.anzFahrgast;
	delete[] Fahrgaeste;
	Fahrgaeste = new Fahrgast[max];
	for(int i = 0; i < anzFahrgast; i++) {
		Fahrgaeste[i] = s.Fahrgaeste[i];
	}
	return *this;
}

Strassenbahn::~Strassenbahn() {
	delete[] Fahrgaeste;
}

void Strassenbahn::fahren() {
	cout << Nummer << ": Ich fahre." << endl;
}

void Strassenbahn::halten() {
	cout << Nummer << ": Ich halte." << endl;
}

int Strassenbahn::getNummer() {
	return Nummer;
}

Fahrgast Strassenbahn::getFahrgast(int pos) {
	return Fahrgaeste[pos];
}

int Strassenbahn::getAnzFahrgast() {
	return anzFahrgast;
}

void Strassenbahn::einsteigen(Fahrgast fg) {
	Fahrgaeste[anzFahrgast++] = fg;
	cout << fg.getName() << " steigt ein" << endl;
}
