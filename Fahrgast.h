/*
 * Fahrgast.h
 *
 *  Created on: 17.07.2018
 *      Author: Hendrik
 */

#ifndef FAHRGAST_H_
#define FAHRGAST_H_

#include "Person.h"

class Fahrgast : public Person {
private:
	bool hatTicket;
	bool kontrolliert;
public:
	Fahrgast(bool ticket);
	Fahrgast();
	void kaufTicket();
	void setKontrolliert(bool b);
	bool getKontrolliert();
	bool getHatTicket();
};

#endif /* FAHRGAST_H_ */
